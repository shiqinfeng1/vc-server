package main

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	_ "github.com/gogf/gf/contrib/drivers/mysql/v2"
	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/util/gmode"
	"github.com/joho/godotenv"
)

func init() {
	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	if err := godotenv.Load(dir + "/.env"); err != nil {
		log.Fatal("load .env: ", err)
	}
}

type redisCheckFunc func(r *gredis.Redis) bool

// checkRedis 检查redis是否可用
func checkRedis(ctx context.Context, cfg *gredis.Config, check redisCheckFunc) {
	redis, err := gredis.New(cfg)
	if err != nil {
		log.Fatal("new redis client: ", err)
	}
	if !check(redis) {
		log.Fatal("connect redis fail")
	}
	log.Println("[redis] connect ok:", cfg.Address)
}

func createDB(ctx context.Context, dbName, user string) {
	db, err := gdb.NewByGroup("default")
	if err != nil {
		log.Fatal("new mysql client: ", err)
	}
	if _, err := db.Exec(ctx, fmt.Sprintf("CREATE DATABASE IF NOT EXISTS  `%s` DEFAULT CHARSET utf8mb4  COLLATE utf8mb4_general_ci", dbName)); err != nil {
		log.Fatal("exec sql: ", err)
	}
	if _, err := db.Exec(ctx, fmt.Sprintf("grant all privileges on *.* to '%s'@'%%' with grant option", user)); err != nil {
		log.Fatal("exec sql:: ", err)
	}
	if _, err := db.Exec(ctx, "set global max_connections=1000"); err != nil {
		log.Fatal("exec sql: ", err)
	}
	if _, err := db.Exec(ctx, "set global thread_cache_size=200"); err != nil {
		log.Fatal("exec sql: ", err)
	}
	// if _, err := db.Exec(ctx, "set global slow_query_log='ON'"); err != nil {
	// 	log.Fatal("exec sql: ", err)
	// }
	if _, err := db.Exec(ctx, "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))"); err != nil {
		log.Fatal("exec sql: ", err)
	}
	if _, err := db.Exec(ctx, "set global long_query_time=0.5"); err != nil {
		log.Fatal("exec sql: ", err)
	}
	log.Println("[mysql] create ok:", dbName)
}

// 读取sql文件
func readSqlFile(path string) (sqlArr []string, err error) {

	inputFile, inputError := os.Open(path)
	if inputError != nil {
		return nil, inputError
	}
	defer inputFile.Close()
	inputReader := bufio.NewReader(inputFile)
	var (
		flag   bool = true
		buffer bytes.Buffer
	)

	for {
		inputString, readerError := inputReader.ReadString('\n')
		inputString = strings.TrimSpace(inputString)
		if strings.HasPrefix(inputString, "--") {
			flag = false
		}
		if strings.HasPrefix(inputString, "/*") {
			flag = false
		}

		if flag && inputString != "" {
			if strings.HasSuffix(inputString, ";") {
				if buffer.Len() == 0 {
					sqlArr = append(sqlArr, inputString)
				} else {
					buffer.WriteString(inputString)
					sqlArr = append(sqlArr, buffer.String())
					buffer.Reset()
				}
			} else {
				buffer.WriteString(inputString + " ")
			}
		}

		if !flag && strings.HasPrefix(inputString, "*/") {
			flag = true
		}

		if !flag && strings.HasPrefix(inputString, "--") {
			flag = true
		}

		if readerError == io.EOF {
			break
		}
	}
	return
}

// 创建数据库表
func importDBTable(ctx context.Context, dbName, sqlFilePath string) (err error) {
	db, err := gdb.NewByGroup(dbName)
	if err != nil {
		log.Fatal("new mysql client: ", err)
	}
	sqlArr, err := readSqlFile(sqlFilePath)
	if err != nil {
		log.Fatal("read sql fle: ", err)
	}

	if len(sqlArr) == 0 {
		log.Fatal("read sql fle: sqlArr is empty")
		return
	}

	for _, item := range sqlArr {
		_, err = db.Exec(ctx, item)
		if err != nil {
			log.Fatal("read sql fle: ", err)
		}
	}
	log.Println("[mysql] import ok:", sqlFilePath)
	return
}

// initDB 创建数据库，并导入数据表及初始数据
func initDB(ctx context.Context, cfg gdb.Config, user string) {
	gdb.SetConfig(cfg)
	createDB(ctx, "chainvcdb", user)
	importDBTable(ctx, "chainvcdb", "resource/chainvcdb.sql")
}

func checkDataSource() {
	ctx := gctx.New()
	hostIP := os.Getenv("HOST_IP")
	redisP := os.Getenv("REDIS_SRV_PORT")
	redisSP := os.Getenv("REDIS_SLAVE_SRV_PORT")
	redisPass := os.Getenv("REDIS_PASSWOED")
	mysqlP := os.Getenv("MYSQL_PORT")
	mysqlRoot := os.Getenv("MYSQL_ROOT_USER")
	mysqlPwd := os.Getenv("MYSQL_ROOT_PASSWORD")
	mysqlUser := os.Getenv("MYSQL_USER")

	rc := &gredis.Config{
		Address: hostIP + ":" + redisP,
		Db:      0,
		Pass:    redisPass,
	}
	rsc := &gredis.Config{
		Address: hostIP + ":" + redisSP,
		Db:      0,
		Pass:    redisPass,
	}
	checkRedis(ctx, rc, func(r *gredis.Redis) bool {
		v, err := r.Do(ctx, "setex", "HIT:REDIS", 10, "alive")
		if err != nil {
			log.Println(err)
			return false
		}
		if v != nil && v.String() == "OK" {
			return true
		}
		return false
	})
	time.Sleep(time.Second)
	checkRedis(ctx, rsc, func(r *gredis.Redis) bool {
		v, err := r.Do(ctx, "get", "HIT:REDIS")
		if err != nil {
			log.Println(err)
			return false
		}
		if v != nil && v.String() == "alive" {
			return true
		}
		return false
	})

	mc := gdb.Config{
		"default": gdb.ConfigGroup{
			gdb.ConfigNode{
				Host:   hostIP,
				Port:   mysqlP,
				User:   mysqlRoot,
				Pass:   mysqlPwd,
				Type:   "mysql",
				Role:   "master",
				Weight: 100,
			},
		},
		"chainvcdb": gdb.ConfigGroup{
			gdb.ConfigNode{
				Host:   hostIP,
				Port:   mysqlP,
				User:   mysqlRoot,
				Pass:   mysqlPwd,
				Name:   "chainvcdb",
				Type:   "mysql",
				Role:   "master",
				Weight: 100,
			},
		},
	}
	if gmode.Mode() == gmode.DEVELOP {
		initDB(ctx, mc, mysqlUser)
	}
}
