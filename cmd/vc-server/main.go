package main

import (
	"os"

	v1 "chainmaker.org/shiqinfeng1/vc-server/api/vc/v1"
	"chainmaker.org/shiqinfeng1/vc-server/internal/hashcard"
	"chainmaker.org/shiqinfeng1/vc-server/internal/service"
	"github.com/gogf/gf/contrib/rpc/grpcx/v2"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/util/gmode"
	"google.golang.org/grpc"
)

func init() {
	ctx := gctx.New()

	mode := g.Cfg().MustGet(ctx, "mode").String()
	switch mode {
	case "develop":
		gmode.SetDevelop()
	case "product":
		gmode.SetProduct()
	default:
		panic("[系统初始化] 系统运行模式未知")
	}

	checkDataSource()

	// 初始化硬件hash板卡
	g.Log().Infof(ctx, "[系统初始化] HASHCARD=%v ", os.Getenv("HASHCARD"))
	if os.Getenv("HASHCARD") != "NOTOK" {
		hashcard.InitHashCard(ctx)
		hashcard.InitRngCard(ctx)
	}

	g.Log().Info(ctx, "[系统初始化] 完成！ ")
}

func main() {
	ctx := gctx.New()

	// 启动grpc服务

	c := grpcx.Server.NewConfig()
	c.Options = append(c.Options, []grpc.ServerOption{
		grpcx.Server.ChainUnary(
		// grpcx.Server.UnaryValidate,
		// grpctoken.ServerInterceptorCheckToken(),
		)}...,
	)
	s := grpcx.Server.New(c)
	v1.RegisterVcServer(s.Server, service.NewVcService())
	s.Start()
	s.Wait()

	// 如果需要处理善后，在这里添加
	g.Log().Info(ctx, "Shutdown Server ...")

	if os.Getenv("HASHCARD") != "NOTOK" {
		hashcard.DeInitHashCard(ctx)
		hashcard.DeInitRngCard(ctx)
	}
	g.Log().Info(ctx, "VC-Server exited ok")
}
