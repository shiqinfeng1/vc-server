/***
 * @FilePath     : mtqrng.h
 * @Description  :The SDK interface file of random number board under Linux system,
 *                which can output bytes of quantum random number by calling the output interface.
 * @Author       : lishen
 * @Version      : V1.1.1
 * @Date         : 2022-07-08 08:57:14
 * @LastEditTime : 2022-10-17 09:00:19
 * @email        : lishen@jmail.com
 * @
 * @CopyRight    : Copyright (c) 2022 by MatrixTime Digit Technology Co. Ltd, All Rights Reserved.
 */
#ifndef __MTQRNG_H__
#define __MTQRNG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

// Serial port communication device descriptor, which is used for setting board parameters and querying status.
#define DEVICE_UART "/dev/ttyUSB0"

    /***
     * @description: get number of qrng pcie devices
     * @return : <=0: no device available, >0: n devices available
     */
    int mtqrng_pcie_get_count(void);

    /***
     * @description: Open and return an available qrng PCIe device.
     * @param :int  devid: -1: Open the device file automatically assigned by the system,
     * 					   0~mtqrng_pcie_dev_init return value: open the device specified by user input.
     * @return : -1:failed; >=0:succeed
     */
    int mtqrng_pcie_open(int devid);

    /**
     * @description: close opened device
     * @param :int  devid: device id
     * @return :
     */
    void mtqrng_pcie_close(int devid);

    /***
     * @description: get random from pcie interface
     * @param :int  devid: device id
     * @param :unsigned char  *outdata: out data of random number
     * @param :unsigned int len: random number length needed to be read
     * @return : -1: failed, >0: return real read length
     */
    int mtqrng_pcie_read_random(int devid, unsigned char *outdata, unsigned int len);

    /***
     * @description: qrng device status query interface
     * @param :int  devid: device id
     * @param :const char *ufile: device uart file name in the host, defined "DEVICE_UART" in mtqrng.h
     * @param :char  *dst: the value of PCIe device, 13 bytes, defined as follow:
     *			      单板温度：dst[0:1]/ 100 ℃
     *				  芯片温度：dst[2:3] / 100 ℃
     *				  激光器温度：dst[4:5] / 100 ℃
     *				  激光器电流：dst[6:7] / 100 A
     *				  最小熵：dst[8:9] / 100
     *				  当前运行模式：dst[10] =0：输出关闭；=1：PCIe输出；=2：SLED光口输出；=3：GuopMi光口输出
     *				  熵源自检结果：dst[11] =0x55：通过；0x00：正在检测； 其他：熵源检测未通过失效
     *				  上电自检信息：dst[12] =0xAx：不通过；其他：通过
     *				  周期自检信息：dst[13] 最高位bit_7为1：不通过；其他：通过
     *				  系统自检结果：dst[14:15]：系统检测结果
     *							   bit_0: 1,正常；
     *							   bit_1: 1,光路错误；
     *							   bit_2: 1,网络未连接；
     *							   bit_3: 1,PCIe未连接；
     *							   bit_4: 1,单板温度过高；
     *							   bit_5: 1,FPGA温度过高；
     *							   bit_6: 1,激光器温度异常；
     *							   bit_7: 1,TEC电流异常；
     *							   bit_8: 1,最小熵异常；
     *							   bit_9: 1,熵源数据检测未通过；
     *							   bit_10: 1,上电数据检测未通过；
     *							   bit_11: 1,周期数据检测未通过；
     *							   bit_12: 1,接收光模块功率异常；
     *							   bit_13: 1,接收光模块温度异常；
     *							   bit_14: 1,板卡故障输出异常；
     *							   bit_15: 1,设备已启动初始化完成；0,设备正在初始化；
     *				  版本号：VX.Y.Z, X is dst[16]&0x0F, Y is dst[17]>>4 & 0x0F, Z is dst[17]&0x0F
     * @return : 0: 串口通信正常，获取到状态,
     * 			 -1: 串口通信异常常，未获取到状态,
     */
    int mtqrng_pcie_get_devstatus(int devid, const char *ufile, char *dst);

    /**
     * @description: reset pcie device status, calling this SDK interface will automatically software reset the device.
     * @param :int  devid: device id
     * @param :char  *ufile: device uart file name in the host, defined "DEVICE_UART" in mtqrng.h
     * @return : -1:failed; 0:succeed
     */
    int mtqrng_pcie_dev_reset(int devid, const char *ufile);

    /**
     * @description: set pcie device output model.
     * @param :int  devid: device id
     * @param :char  *ufile: device uart file name in the host, defined "DEVICE_UART" in mtqrng.h
     * @param :int  mode: 0:close；1:PCIe；2:net
     * @return : -1:failed; 0:succeed
     */
    int mtqrng_pcie_model_set(int devid, const char *ufile, int mode);

    /**
     * @description: set pcie device net ip port MAC.
     * @param :int  devid: device id
     * @param :char  *ufile: device uart file name in the host, defined "DEVICE_UART" in mtqrng.h
     * @param :char *npar: npar[0~3]: device ip info, npar[4~5]:device port info, npar[6~11]:device MAC info
     * 					   npar[12~15]: local ip info, npar[16~17]: local port info
     * @return : -1:failed; 0:succeed
     */
    int mtqrng_pcie_netip_set(int devid, const char *ufile, char *npar);

    /***
     * @description: get qrng pcie sdk version
     * @param :char  *libinfo: this needs 6 bytes of memory space
     * @return :
     */
    void mtqrng_pcie_get_libraryinfo(char *libinfo);

    /***
     * @description: get qrng pcie driver version
     * @param :char  *driverinfo: this needs 6 bytes of memory space
     * @return :
     */
    void mtqrng_pcie_get_driverinfo(char *driverinfo);

#ifdef __cplusplus
}
#endif
#endif
