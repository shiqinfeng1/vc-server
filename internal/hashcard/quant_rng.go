package hashcard

/*
#cgo CFLAGS: -I./lib  -mavx2
#cgo LDFLAGS: -L./lib -lmtqrng  -lrt -lm -ldl
#include "./lib/mtqrng.h"
#include "./lib/pfunc.h"

#define DEVICE_UART "/dev/ttyUSB0"

static int rngdevID = -1;
int InitRng(char* sover, char* xdmaver)
{
    int32_t num =  mtqrng_pcie_get_count();
    if (num <= 0)
        return -97;

	rngdevID = mtqrng_pcie_open(-1);
	if (rngdevID < 0)
		return -98;

    mtqrng_pcie_get_libraryinfo(sover);
    mtqrng_pcie_get_driverinfo(xdmaver);

    return 1;
}
int GetRngHw(int rngLen, char *outRng)
{
	int iRet = mtqrng_pcie_read_random(rngdevID, outRng, rngLen);
	if (iRet < 0)
		return iRet;
	return 0;
}
void DeInitRng()
{
    mtqrng_pcie_close(rngdevID);
    return;
}

*/
import "C"
import (
	"context"
	"encoding/hex"
	"fmt"
	"math/rand"
	"unsafe"

	"github.com/gogf/gf/v2/frame/g"
)

var isSupportRngCard bool

func InitRngCard(ctx context.Context) {
	var soversion, xdmaversion [8]byte
	if ret := C.InitRng((*C.char)(unsafe.Pointer(&soversion[0])), (*C.char)(unsafe.Pointer(&xdmaversion[0]))); ret < 0 {
		g.Log().Warningf(ctx, "初始化随机数板卡失败 ret=%v\n", ret)
		isSupportRngCard = false
	} else {
		// g.Log().Infof(ctx,"初始化随机数板卡成功 soVer=%s xdmaVer=%s\n", string(soversion[:]), string(xdmaversion[:]))
		g.Log().Info(ctx, "初始化随机数板卡成功\n")
		isSupportRngCard = true
	}
}
func DeInitRngCard(ctx context.Context) {
	if isSupportRngCard {
		C.DeInitRng()
		g.Log().Info(ctx, "反初始化随机数板卡完成\n")
	}
}

func GetRngHw(ctx context.Context, rngLlen uint) (outRng []byte, err error) {
	outRng = make([]byte, rngLlen)
	if ret := C.GetRngHw((C.int)(rngLlen), (*C.char)(unsafe.Pointer(&outRng[0]))); ret < 0 {
		err = fmt.Errorf("获取量子随机数失败. ret=%v", ret)
		return
	}
	g.Log().Debugf(ctx, "获取量子随机数=%v", hex.EncodeToString(outRng))
	return
}

// hash的长度为16字节
func GetRng(ctx context.Context, rngLlen uint) (outRng []byte, err error) {
	if isSupportRngCard {
		return GetRngHw(ctx, rngLlen)
	} else {
		ran := randomString(rngLlen, 3)
		g.Log().Debugf(ctx, "获取伪随机数=%v", hex.EncodeToString(ran))
		return ran, nil
	}
}

/*
*
  - size 随机码的位数
  - kind 0    // 纯数字
    1    // 小写字母
    2    // 大写字母
    3    // 数字、大小写字母
*/
func randomString(size uint, kind int) []byte {
	ikind := kind
	kinds := [][]int{{10, 48}, {26, 97}, {26, 65}}
	rsbytes := make([]byte, size)

	isAll := kind > 2 || kind < 0

	for i := 0; i < int(size); i++ {
		if isAll { // random ikind
			ikind = rand.Intn(3)
		}
		scope, base := kinds[ikind][0], kinds[ikind][1]
		rsbytes[i] = uint8(base + rand.Intn(scope))
	}
	return rsbytes
}
