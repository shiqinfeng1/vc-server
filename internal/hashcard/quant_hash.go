package hashcard

/*
#cgo CFLAGS: -I./lib   -mavx2
#cgo LDFLAGS: -L./lib -lquant_hash -lrt -lm -ldl -lmthash
#include "./lib/mthash.h"
#include "./lib/pfunc.h"
#include "./lib/QuantHash.h"
#include <stdio.h>

void GetP(char* pRand, char* outP)
{
	Init();
	pRand[0] |= 1;
	pRand[16] = 1;
    int32_t iRet = GetGF(pRand, outP);
    if (0 != iRet)
    {
        printf("get p fail. ret=%d:\n",iRet);
        return;
    }
	return;
}

// 计算hash值
void GetHash(char *pRand, char *P, char *pBuf, uint64_t iLen, char *outHash)
{
	Init();
    QuantumHashCommon(pBuf, iLen, pRand, P, outHash);
	return;
}

// 下面是hash板卡实现的接口  -lpthread

#define KEY_LEN 16
#define HASH_LEN 16

static int devID = -1;
int InitHW(char* sover, char* xdmaver)
{
	// 李绅新版本 不再需要调用该接口 2022-12-05
    // int32_t num = mthash_pcie_dev_init();
    // if (num <= 0)
    //     return num;

	devID = mthash_pcie_open(-1);
	if (devID < 0)
		return devID;

    mthash_pcie_get_libraryinfo(sover);
    mthash_pcie_get_driverinfo(xdmaver);
    return 1;
}
void GetHashHw(char *pRand, char *P, char *pBuf, uint64_t iLen, char *outHash)
{
	Mt_Data_Type_s tDoc;
    tDoc.len = iLen;
    tDoc.data = (uint8_t *)pBuf;

	Mt_Para_Type_s tHashParam;
    tHashParam.hashLen = KEY_LEN;
    tHashParam.hashKey = (uint8_t*)pRand;
    tHashParam.polyLen = KEY_LEN;
    tHashParam.polyPara =  (uint8_t*)P;

	Mt_Data_Type_s _pRst;
	_pRst.len = 0;
    _pRst.data = (uint8_t *)outHash;
	mthash_pcie_get_hashvalue(devID, &tDoc, &tHashParam, &_pRst);

	return;
}
void DeInit()
{
    mthash_pcie_close(devID);
    return;
}

*/
import "C"
import (
	"context"
	"encoding/hex"
	"sync"
	"time"
	"unsafe"

	"github.com/gogf/gf/v2/frame/g"
)

var isSupportHashCard bool

const ContextsMaxLen = 1024

func InitHashCard(ctx context.Context) {
	var soversion, xdmaversion [8]byte
	if ret := C.InitHW((*C.char)(unsafe.Pointer(&soversion[0])), (*C.char)(unsafe.Pointer(&xdmaversion[0]))); ret < 0 {
		g.Log().Warningf(ctx, "初始化hash板卡失败 ret=%v\n", ret)
		isSupportHashCard = false
	} else {
		// g.Log().Infof(ctx,"初始化hash板卡成功 soVer=%s xdmaVer=%s\n", string(soversion[:]), string(xdmaversion[:]))
		g.Log().Info(ctx, "初始化hash板卡成功\n")
		isSupportHashCard = true
	}
}
func DeInitHashCard(ctx context.Context) {
	if isSupportHashCard {
		C.DeInit()
		g.Log().Info(ctx, "反初始化hash板卡完成\n")
	}
}
func GetP(ctx context.Context) []byte {
	// var (
	// 	r16 []byte
	// 	err error
	// )
	// if r16, err = GetRng(ctx, 16); err != nil {
	// 	g.Log().Errorf(ctx, "获取随机数r16失败. err=%v", err)
	// 	return nil
	// }

	temp := [16]byte{0x95, 0x9d, 0xf1, 0x41, 0x16, 0x22, 0x4c, 0xfa, 0x75, 0xeb, 0x51, 0xac, 0x86, 0x47, 0xa7, 0x61} //0x959df14116224cfa75eb51ac8647a761
	// temp := [16]byte{0xFF, 0x4B, 0xBE, 0xB2, 0x53, 0x3E, 0xF4, 0xAE, 0xF5, 0x4B, 0xFA, 0x26, 0x54, 0xFF, 0x3C, 0x0A} //0xFF4BBEB2533EF4AEF54BFA2654FF3C0B
	// temp := [16]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01, 0x86, 0x96} //0x018697
	r16 := reverse(temp[:])

	zero := [16]byte{}
	r32 := append(r16[:], zero[:]...)
	g.Log().Debug(ctx, "计算不可约的输入随机数=", hex.EncodeToString(reverse(r32[:])))
	t1 := time.Now()
	var p [32]byte
	C.GetP((*C.char)(unsafe.Pointer(&r32[0])), (*C.char)(unsafe.Pointer(&p[0])))
	t2 := time.Since(t1)
	p16 := reverse(p[:16])
	g.Log().Debug(ctx, "得到不可约多项式=", hex.EncodeToString(p16), "time=", t2)
	return p16
}

// hash的长度为16字节
func GetHash(ctx context.Context, p, s, buf []byte, len uint64) []byte {
	if isSupportHashCard && len > ContextsMaxLen {
		return GetHashHw(ctx, p, s, buf, len)
	} else {
		return GetHashSw(ctx, p, s, buf, len)
	}
}

var mux sync.Mutex

func GetHashSw(ctx context.Context, p, s, buf []byte, len uint64) []byte {
	p16 := reverse(p[:16])
	var hash [16]byte
	mux.Lock()
	C.GetHash((*C.char)(unsafe.Pointer(&s[0])),
		(*C.char)(unsafe.Pointer(&p16[0])),
		(*C.char)(unsafe.Pointer(&buf[0])),
		(C.ulong)(len),
		(*C.char)(unsafe.Pointer(&hash[0])))
	mux.Unlock()
	h16 := reverse(hash[:16])
	// g.Log().Debugf(ctx, "h16=", hex.EncodeToString(h16[:]))
	return h16
}

func GetHashHw(ctx context.Context, p, s, buf []byte, len uint64) []byte {
	p16 := reverse(p[:16])
	var hash [16]byte
	C.GetHashHw((*C.char)(unsafe.Pointer(&s[0])),
		(*C.char)(unsafe.Pointer(&p16[0])),
		(*C.char)(unsafe.Pointer(&buf[0])),
		(C.ulong)(len),
		(*C.char)(unsafe.Pointer(&hash[0])))
	h16 := reverse(hash[:16])
	// g.Log().Debugf(ctx, "h16=", hex.EncodeToString(h16[:]))
	return h16
}
