// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VcCrtStorageDao is the data access object for table vc_crt_storage.
type VcCrtStorageDao struct {
	table   string              // table is the underlying table name of the DAO.
	group   string              // group is the database configuration group name of current DAO.
	columns VcCrtStorageColumns // columns contains all the column names of Table for convenient usage.
}

// VcCrtStorageColumns defines and stores column names for table vc_crt_storage.
type VcCrtStorageColumns struct {
	Id        string //
	KeyId     string // 证书id标识
	KeyPath   string // 私钥路径
	CrtPath   string // 证书路径
	CreatedAt string // 创建时间
	UpdatedAt string // 更新时间
}

// vcCrtStorageColumns holds the columns for table vc_crt_storage.
var vcCrtStorageColumns = VcCrtStorageColumns{
	Id:        "id",
	KeyId:     "key_id",
	KeyPath:   "key_path",
	CrtPath:   "crt_path",
	CreatedAt: "created_at",
	UpdatedAt: "updated_at",
}

// NewVcCrtStorageDao creates and returns a new DAO object for table data access.
func NewVcCrtStorageDao() *VcCrtStorageDao {
	return &VcCrtStorageDao{
		group:   "default",
		table:   "vc_crt_storage",
		columns: vcCrtStorageColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VcCrtStorageDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VcCrtStorageDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VcCrtStorageDao) Columns() VcCrtStorageColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VcCrtStorageDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VcCrtStorageDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VcCrtStorageDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
