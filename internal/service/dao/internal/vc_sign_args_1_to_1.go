// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VcSignArgs1To1Dao is the data access object for table vc_sign_args1to1.
type VcSignArgs1To1Dao struct {
	table   string                // table is the underlying table name of the DAO.
	group   string                // group is the database configuration group name of current DAO.
	columns VcSignArgs1To1Columns // columns contains all the column names of Table for convenient usage.
}

// VcSignArgs1To1Columns defines and stores column names for table vc_sign_args1to1.
type VcSignArgs1To1Columns struct {
	Id        string //
	Sotid     string // otid的签名参数s
	Totid     string // otid的签名参数t
	Sac       string // ac的签名参数s
	Tac       string // ac的签名参数t
	Otid      string // otid值
	TxnId     string // 交易id
	CreatedAt string // 创建时间
	UpdatedAt string // 更新时间
}

// vcSignArgs1To1Columns holds the columns for table vc_sign_args1to1.
var vcSignArgs1To1Columns = VcSignArgs1To1Columns{
	Id:        "id",
	Sotid:     "sotid",
	Totid:     "totid",
	Sac:       "sac",
	Tac:       "tac",
	Otid:      "otid",
	TxnId:     "txn_id",
	CreatedAt: "created_at",
	UpdatedAt: "updated_at",
}

// NewVcSignArgs1To1Dao creates and returns a new DAO object for table data access.
func NewVcSignArgs1To1Dao() *VcSignArgs1To1Dao {
	return &VcSignArgs1To1Dao{
		group:   "default",
		table:   "vc_sign_args1to1",
		columns: vcSignArgs1To1Columns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VcSignArgs1To1Dao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VcSignArgs1To1Dao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VcSignArgs1To1Dao) Columns() VcSignArgs1To1Columns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VcSignArgs1To1Dao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VcSignArgs1To1Dao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VcSignArgs1To1Dao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
