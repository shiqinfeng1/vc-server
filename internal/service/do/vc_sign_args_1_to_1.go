// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VcSignArgs1To1 is the golang structure of table vc_sign_args1to1 for DAO operations like Where/Data.
type VcSignArgs1To1 struct {
	g.Meta    `orm:"table:vc_sign_args1to1, do:true"`
	Id        interface{} //
	Sotid     interface{} // otid的签名参数s
	Totid     interface{} // otid的签名参数t
	Sac       interface{} // ac的签名参数s
	Tac       interface{} // ac的签名参数t
	Otid      interface{} // otid值
	TxnId     interface{} // 交易id
	CreatedAt *gtime.Time // 创建时间
	UpdatedAt *gtime.Time // 更新时间
}
