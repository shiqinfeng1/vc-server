// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VcCrtStorage is the golang structure of table vc_crt_storage for DAO operations like Where/Data.
type VcCrtStorage struct {
	g.Meta    `orm:"table:vc_crt_storage, do:true"`
	Id        interface{} //
	KeyId     interface{} // 证书id标识
	KeyPath   interface{} // 私钥路径
	CrtPath   interface{} // 证书路径
	CreatedAt *gtime.Time // 创建时间
	UpdatedAt *gtime.Time // 更新时间
}
