package service

import (
	"bytes"
	"context"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"errors"
	"os"
	"time"

	v1 "chainmaker.org/shiqinfeng1/vc-server/api/vc/v1"
	"chainmaker.org/shiqinfeng1/vc-server/internal/base"
	"chainmaker.org/shiqinfeng1/vc-server/internal/hashcard"
	"chainmaker.org/shiqinfeng1/vc-server/internal/model/entity"
	"chainmaker.org/shiqinfeng1/vc-server/internal/service/dao"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

const CacheTagVcSignArgsTag1To1 = "vcSiginArgsTag1To1"
const CacheTagVcSignPrivKey1To1 = "vcSiginPrivKeyTag1To1"

// VcService is a greeter service.
type VcService struct {
	v1.UnimplementedVcServer
}

// NewVcService new a greeter service.
func NewVcService() *VcService {
	return &VcService{}
}

func (vcs *VcService) checkOTCA1To1(ctx context.Context, otcaHex, totidHex, sotidHex string, priveKey []byte) error {

	// 用t解密OTCA
	otca, err := hex.DecodeString(otcaHex)
	if err != nil {
		return gerror.Newf("VC校验OTCA1to1: 参数MacOTCA无效")

	}
	totid, err := hex.DecodeString(totidHex)
	if err != nil {
		return gerror.Newf("VC校验OTCA1to1: 参数Totid无效")
	}
	if len(otca) != 32 || len(totid) != 32 {
		return gerror.New("VC校验OTCA1to1: 参数otca/totid长度非法(otca/totid) len !=32")
	}
	tmp, _ := hashcard.Xor(otca, totid)
	hashPrivid := tmp[:16]
	Potid := tmp[16:]

	// 计算隐私身份的hash
	sotid, _ := hex.DecodeString(sotidHex)
	myHashPrivid := hashcard.GetHash(ctx, Potid, sotid, priveKey, uint64(len(priveKey)))
	if bytes.Equal(hashPrivid, myHashPrivid) {
		return nil
	}
	return gerror.New("VC校验OTCA1to1: hash不匹配")
}
func (vcs *VcService) getKey(ctx context.Context, publicKey string) ([]byte, error) {
	var storj *entity.VcCrtStorage
	keyId := hashcard.SHA256([]byte(publicKey))

	privkey := base.Cache().Get(ctx, keyId)
	if privkey == nil || len(privkey.Bytes()) == 0 {
		dao.VcCrtStorage.Ctx(ctx).Where(dao.VcCrtStorage.Columns().KeyId, keyId).Scan(&storj)
		raw, err := os.ReadFile(storj.KeyPath)
		if err != nil {
			return nil, err
		}
		if len(raw) <= 0 {
			return nil, errors.New("PEM is nil")
		}
		block, _ := pem.Decode(raw)
		if block == nil || block.Type != "JZ PRIVATE KEY" {
			return nil, errors.New("PEM is invald")
		}
		key, err := x509.ParseECPrivateKey(block.Bytes)
		if err != nil {
			return nil, errors.New("privkey is invald")
		}
		privkey.Set(key.X.Bytes())
		base.Cache().Set(ctx, keyId, privkey.Bytes(), time.Duration(24*time.Hour), CacheTagVcSignPrivKey1To1)
	}

	return privkey.Bytes(), nil
}

func (vcs *VcService) SaveSign1To1Args(ctx context.Context, req *v1.SaveSign1To1ArgsReq) (*v1.SaveSign1To1ArgsRes, error) {

	res := new(v1.SaveSign1To1ArgsRes)

	privkey, err := vcs.getKey(ctx, req.PublicKey)
	if err != nil {
		return nil, err
	}

	g.Log().Debugf(ctx, "[SaveSign1To1Args]: %s ", req.String())
	signArgs := &entity.VcSignArgs1To1{
		Sotid: req.Sotid,
		Totid: req.Totid,
		Sac:   req.Sac,
		Tac:   req.Tac,
		Otid:  req.OTID,
		TxnId: req.TxnID,
	}
	msignArgs := base.Cache().Get(ctx, req.TxnID)
	if msignArgs == nil || len(msignArgs.Bytes()) == 0 || json.Unmarshal(msignArgs.Bytes(), &signArgs) != nil {
		_, err = dao.VcSignArgs1To1.Ctx(ctx).Insert(signArgs)
		if err != nil {
			return nil, err
		}
		msignArgs2, _ := json.Marshal(signArgs)
		base.Cache().Set(ctx, req.TxnID, msignArgs2, time.Duration(24*time.Hour), CacheTagVcSignArgsTag1To1)
	}

	err = vcs.checkOTCA1To1(ctx, req.Otca, req.Totid, req.Sotid, privkey)
	if err != nil {
		g.Log().Debugf(ctx, "[checkOTCA1To1]: %v ", err)
		return nil, err
	}

	res.Result = "success"
	return res, nil

}
func (vcs *VcService) Verify1To1(ctx context.Context, req *v1.Verify1To1Req) (*v1.Verify1To1Res, error) {
	res := new(v1.Verify1To1Res)
	// 验证sha3
	sab, err := hex.DecodeString(req.Sab)
	if err != nil {
		g.Log().Errorf(ctx, "req=%+v err=%v", req, err)
		return nil, gerror.Newf("VC验签: 参数Sab无效")
	}
	tab, err := hex.DecodeString(req.Tab)
	if err != nil {
		g.Log().Errorf(ctx, "req=%+v err=%v", req, err)
		return nil, gerror.Newf("VC验签: 参数Tab无效")
	}
	if len(sab) != 16 || len(tab) != 32 {
		g.Log().Errorf(ctx, "req=%+v", req)
		return nil, gerror.New("VC验签: 参数Sab/Tab长度非法")
	}
	if req.HashSab != hashcard.SHA256(sab) || req.HashTab != hashcard.SHA256(tab) {
		g.Log().Errorf(ctx, "req=%+v", req)
		return nil, gerror.New("VC验签: 参数完整性校验未通过")
	}

	// 查询签名参数
	var signArgs entity.VcSignArgs1To1
	msignArgs := base.Cache().Get(ctx, req.TxnID)
	if msignArgs == nil || len(msignArgs.Bytes()) == 0 || json.Unmarshal(msignArgs.Bytes(), &signArgs) != nil {
		err = dao.VcSignArgs1To1.Ctx(ctx).Where(dao.VcSignArgs1To1.Columns().TxnId, req.TxnID).Scan(&signArgs)
		if err != nil {
			g.Log().Errorf(ctx, "req=%+v", req)
			return nil, gerror.New("VC验签: 未找到签名参数")
		}
		msignArgs2, _ := json.Marshal(signArgs)
		base.Cache().Set(ctx, req.TxnID, msignArgs2, time.Duration(24*time.Hour), CacheTagVcSignArgsTag1To1)
	}

	// 用t解密MACdata
	tac, e := hex.DecodeString(signArgs.Tac)
	if e != nil {
		g.Log().Errorf(ctx, "req=%+v err=%v", req, e)
		return nil, gerror.Newf("VC验签: 参数Tac无效")
	}
	sac, e := hex.DecodeString(signArgs.Sac)
	if e != nil {
		g.Log().Errorf(ctx, "req=%+v err=%v", req, e)
		return nil, gerror.Newf("VC验签: 参数Sac无效")
	}
	if len(sac) != 16 || len(tac) != 32 {
		g.Log().Errorf(ctx, "req=%+v", req)
		return nil, gerror.New("VC验签: 参数Sac/Tac长度非法")
	}
	macSign, e := hex.DecodeString(req.MacSign)
	if e != nil {
		g.Log().Errorf(ctx, "req=%+v err=%v", req, e)
		return nil, gerror.Newf("VC验签: MacSign无效")
	}
	if len(macSign) != 32 {
		g.Log().Errorf(ctx, "req=%+v", req)
		return nil, gerror.New("VC验签: MacSign无效")
	}
	hashData, e := hex.DecodeString(req.HashData)
	if e != nil {
		g.Log().Errorf(ctx, "req=%+v err=%v", req, e)
		return nil, gerror.Newf("VC验签: hashData无效")
	}
	if len(hashData) != 16 {
		g.Log().Errorf(ctx, "req=%+v", req)
		return nil, gerror.New("VC验签:hashData无效")
	}
	T, _ := hashcard.Xor(tab, tac)
	S, _ := hashcard.Xor(sab, sac)
	tmp, _ := hashcard.Xor(macSign, T)
	hashSign := tmp[:16]
	Psign := tmp[16:]
	// 计算hash的sign
	otid, e := hex.DecodeString(signArgs.Otid)
	if e != nil {
		g.Log().Errorf(ctx, "req=%+v err=%v", req, e)
		return nil, gerror.Newf("VC验签: 参数OTID无效")
	}
	if len(otid) != 16 {
		g.Log().Errorf(ctx, "req=%+v", req)
		return nil, gerror.New("VC验签: 参数OTID无效")
	}
	sign := append(hashData, otid...)
	myHashSign := hashcard.GetHash(ctx, Psign, S, sign, uint64(len(sign)))
	g.Log().Debugf(ctx, "验签用的参数:%v", func() string {
		v := g.Map{
			"OTID":       signArgs.Otid,
			"sign":       hex.EncodeToString(sign),
			"T":          hex.EncodeToString(T),
			"S":          hex.EncodeToString(S),
			"Sac":        signArgs.Sac,
			"Sab":        req.Sab,
			"Tac":        signArgs.Tac,
			"Tab":        req.Tab,
			"Sotid":      signArgs.Sotid,
			"Totid":      signArgs.Totid,
			"hashData":   req.HashData,
			"macSign":    req.MacSign,
			"TxnID":      req.TxnID,
			"Psign":      hex.EncodeToString(Psign),
			"hashSign":   hex.EncodeToString(hashSign),
			"myHashSign": hex.EncodeToString(myHashSign),
		}
		m, _ := json.MarshalIndent(v, "", " ")
		return string(m)
	}())
	// 验证hash是否一致
	if !bytes.Equal(hashSign, myHashSign) {
		g.Log().Errorf(ctx, "req=%+v", req)
		return nil, gerror.New("VC验签: 签名hash校验未通过")
	}

	res.TxnID = req.TxnID
	res.HashSac = hashcard.SHA256(sac)
	res.HashTac = hashcard.SHA256(tac)
	res.Sac = signArgs.Sac
	res.Tac = signArgs.Tac
	res.Otid = signArgs.Otid

	return res, nil
}
