// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VcSignArgs1To1 is the golang structure for table vc_sign_args1to1.
type VcSignArgs1To1 struct {
	Id        uint64      `json:"id"        description:""`
	Sotid     string      `json:"sotid"     description:"otid的签名参数s"`
	Totid     string      `json:"totid"     description:"otid的签名参数t"`
	Sac       string      `json:"sac"       description:"ac的签名参数s"`
	Tac       string      `json:"tac"       description:"ac的签名参数t"`
	Otid      string      `json:"otid"      description:"otid值"`
	TxnId     string      `json:"txnId"     description:"交易id"`
	CreatedAt *gtime.Time `json:"createdAt" description:"创建时间"`
	UpdatedAt *gtime.Time `json:"updatedAt" description:"更新时间"`
}
