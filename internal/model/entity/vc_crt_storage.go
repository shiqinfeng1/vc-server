// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VcCrtStorage is the golang structure for table vc_crt_storage.
type VcCrtStorage struct {
	Id        uint64      `json:"id"        description:""`
	KeyId     string      `json:"keyId"     description:"证书id标识"`
	KeyPath   string      `json:"keyPath"   description:"私钥路径"`
	CrtPath   string      `json:"crtPath"   description:"证书路径"`
	CreatedAt *gtime.Time `json:"createdAt" description:"创建时间"`
	UpdatedAt *gtime.Time `json:"updatedAt" description:"更新时间"`
}
