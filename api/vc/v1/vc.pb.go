// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.31.0
// 	protoc        v4.25.0
// source: vc/v1/vc.proto

package v1

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type VcTokenReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AppId string `protobuf:"bytes,1,opt,name=AppId,proto3" json:"AppId,omitempty"`
}

func (x *VcTokenReq) Reset() {
	*x = VcTokenReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vc_v1_vc_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *VcTokenReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*VcTokenReq) ProtoMessage() {}

func (x *VcTokenReq) ProtoReflect() protoreflect.Message {
	mi := &file_vc_v1_vc_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use VcTokenReq.ProtoReflect.Descriptor instead.
func (*VcTokenReq) Descriptor() ([]byte, []int) {
	return file_vc_v1_vc_proto_rawDescGZIP(), []int{0}
}

func (x *VcTokenReq) GetAppId() string {
	if x != nil {
		return x.AppId
	}
	return ""
}

type VcTokenRes struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Token string `protobuf:"bytes,1,opt,name=Token,proto3" json:"Token,omitempty"`
}

func (x *VcTokenRes) Reset() {
	*x = VcTokenRes{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vc_v1_vc_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *VcTokenRes) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*VcTokenRes) ProtoMessage() {}

func (x *VcTokenRes) ProtoReflect() protoreflect.Message {
	mi := &file_vc_v1_vc_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use VcTokenRes.ProtoReflect.Descriptor instead.
func (*VcTokenRes) Descriptor() ([]byte, []int) {
	return file_vc_v1_vc_proto_rawDescGZIP(), []int{1}
}

func (x *VcTokenRes) GetToken() string {
	if x != nil {
		return x.Token
	}
	return ""
}

type SaveSign1To1ArgsReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TxnID     string `protobuf:"bytes,1,opt,name=TxnID,proto3" json:"TxnID,omitempty"`
	Otca      string `protobuf:"bytes,2,opt,name=Otca,proto3" json:"Otca,omitempty"`
	PublicKey string `protobuf:"bytes,3,opt,name=PublicKey,proto3" json:"PublicKey,omitempty"`
	Sotid     string `protobuf:"bytes,4,opt,name=Sotid,proto3" json:"Sotid,omitempty"`
	Totid     string `protobuf:"bytes,5,opt,name=Totid,proto3" json:"Totid,omitempty"`
	Sac       string `protobuf:"bytes,6,opt,name=Sac,proto3" json:"Sac,omitempty"`
	Tac       string `protobuf:"bytes,7,opt,name=Tac,proto3" json:"Tac,omitempty"`
	OTID      string `protobuf:"bytes,8,opt,name=OTID,proto3" json:"OTID,omitempty"`
}

func (x *SaveSign1To1ArgsReq) Reset() {
	*x = SaveSign1To1ArgsReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vc_v1_vc_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaveSign1To1ArgsReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaveSign1To1ArgsReq) ProtoMessage() {}

func (x *SaveSign1To1ArgsReq) ProtoReflect() protoreflect.Message {
	mi := &file_vc_v1_vc_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaveSign1To1ArgsReq.ProtoReflect.Descriptor instead.
func (*SaveSign1To1ArgsReq) Descriptor() ([]byte, []int) {
	return file_vc_v1_vc_proto_rawDescGZIP(), []int{2}
}

func (x *SaveSign1To1ArgsReq) GetTxnID() string {
	if x != nil {
		return x.TxnID
	}
	return ""
}

func (x *SaveSign1To1ArgsReq) GetOtca() string {
	if x != nil {
		return x.Otca
	}
	return ""
}

func (x *SaveSign1To1ArgsReq) GetPublicKey() string {
	if x != nil {
		return x.PublicKey
	}
	return ""
}

func (x *SaveSign1To1ArgsReq) GetSotid() string {
	if x != nil {
		return x.Sotid
	}
	return ""
}

func (x *SaveSign1To1ArgsReq) GetTotid() string {
	if x != nil {
		return x.Totid
	}
	return ""
}

func (x *SaveSign1To1ArgsReq) GetSac() string {
	if x != nil {
		return x.Sac
	}
	return ""
}

func (x *SaveSign1To1ArgsReq) GetTac() string {
	if x != nil {
		return x.Tac
	}
	return ""
}

func (x *SaveSign1To1ArgsReq) GetOTID() string {
	if x != nil {
		return x.OTID
	}
	return ""
}

type SaveSign1To1ArgsRes struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Result string `protobuf:"bytes,1,opt,name=Result,proto3" json:"Result,omitempty"`
}

func (x *SaveSign1To1ArgsRes) Reset() {
	*x = SaveSign1To1ArgsRes{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vc_v1_vc_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaveSign1To1ArgsRes) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaveSign1To1ArgsRes) ProtoMessage() {}

func (x *SaveSign1To1ArgsRes) ProtoReflect() protoreflect.Message {
	mi := &file_vc_v1_vc_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaveSign1To1ArgsRes.ProtoReflect.Descriptor instead.
func (*SaveSign1To1ArgsRes) Descriptor() ([]byte, []int) {
	return file_vc_v1_vc_proto_rawDescGZIP(), []int{3}
}

func (x *SaveSign1To1ArgsRes) GetResult() string {
	if x != nil {
		return x.Result
	}
	return ""
}

type Verify1To1Req struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TxnID    string `protobuf:"bytes,1,opt,name=TxnID,proto3" json:"TxnID,omitempty"`
	HashData string `protobuf:"bytes,2,opt,name=HashData,proto3" json:"HashData,omitempty"`
	MacSign  string `protobuf:"bytes,3,opt,name=MacSign,proto3" json:"MacSign,omitempty"`
	HashSab  string `protobuf:"bytes,4,opt,name=HashSab,proto3" json:"HashSab,omitempty"`
	HashTab  string `protobuf:"bytes,5,opt,name=HashTab,proto3" json:"HashTab,omitempty"`
	Sab      string `protobuf:"bytes,6,opt,name=Sab,proto3" json:"Sab,omitempty"`
	Tab      string `protobuf:"bytes,7,opt,name=Tab,proto3" json:"Tab,omitempty"`
}

func (x *Verify1To1Req) Reset() {
	*x = Verify1To1Req{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vc_v1_vc_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Verify1To1Req) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Verify1To1Req) ProtoMessage() {}

func (x *Verify1To1Req) ProtoReflect() protoreflect.Message {
	mi := &file_vc_v1_vc_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Verify1To1Req.ProtoReflect.Descriptor instead.
func (*Verify1To1Req) Descriptor() ([]byte, []int) {
	return file_vc_v1_vc_proto_rawDescGZIP(), []int{4}
}

func (x *Verify1To1Req) GetTxnID() string {
	if x != nil {
		return x.TxnID
	}
	return ""
}

func (x *Verify1To1Req) GetHashData() string {
	if x != nil {
		return x.HashData
	}
	return ""
}

func (x *Verify1To1Req) GetMacSign() string {
	if x != nil {
		return x.MacSign
	}
	return ""
}

func (x *Verify1To1Req) GetHashSab() string {
	if x != nil {
		return x.HashSab
	}
	return ""
}

func (x *Verify1To1Req) GetHashTab() string {
	if x != nil {
		return x.HashTab
	}
	return ""
}

func (x *Verify1To1Req) GetSab() string {
	if x != nil {
		return x.Sab
	}
	return ""
}

func (x *Verify1To1Req) GetTab() string {
	if x != nil {
		return x.Tab
	}
	return ""
}

type Verify1To1Res struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TxnID   string `protobuf:"bytes,1,opt,name=TxnID,proto3" json:"TxnID,omitempty"`
	HashSac string `protobuf:"bytes,2,opt,name=HashSac,proto3" json:"HashSac,omitempty"`
	HashTac string `protobuf:"bytes,3,opt,name=HashTac,proto3" json:"HashTac,omitempty"`
	Sac     string `protobuf:"bytes,4,opt,name=Sac,proto3" json:"Sac,omitempty"`
	Tac     string `protobuf:"bytes,5,opt,name=Tac,proto3" json:"Tac,omitempty"`
	Otid    string `protobuf:"bytes,6,opt,name=Otid,proto3" json:"Otid,omitempty"`
}

func (x *Verify1To1Res) Reset() {
	*x = Verify1To1Res{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vc_v1_vc_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Verify1To1Res) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Verify1To1Res) ProtoMessage() {}

func (x *Verify1To1Res) ProtoReflect() protoreflect.Message {
	mi := &file_vc_v1_vc_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Verify1To1Res.ProtoReflect.Descriptor instead.
func (*Verify1To1Res) Descriptor() ([]byte, []int) {
	return file_vc_v1_vc_proto_rawDescGZIP(), []int{5}
}

func (x *Verify1To1Res) GetTxnID() string {
	if x != nil {
		return x.TxnID
	}
	return ""
}

func (x *Verify1To1Res) GetHashSac() string {
	if x != nil {
		return x.HashSac
	}
	return ""
}

func (x *Verify1To1Res) GetHashTac() string {
	if x != nil {
		return x.HashTac
	}
	return ""
}

func (x *Verify1To1Res) GetSac() string {
	if x != nil {
		return x.Sac
	}
	return ""
}

func (x *Verify1To1Res) GetTac() string {
	if x != nil {
		return x.Tac
	}
	return ""
}

func (x *Verify1To1Res) GetOtid() string {
	if x != nil {
		return x.Otid
	}
	return ""
}

var File_vc_v1_vc_proto protoreflect.FileDescriptor

var file_vc_v1_vc_proto_rawDesc = []byte{
	0x0a, 0x0e, 0x76, 0x63, 0x2f, 0x76, 0x31, 0x2f, 0x76, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x05, 0x76, 0x63, 0x2e, 0x76, 0x31, 0x22, 0x22, 0x0a, 0x0a, 0x56, 0x63, 0x54, 0x6f, 0x6b,
	0x65, 0x6e, 0x52, 0x65, 0x71, 0x12, 0x14, 0x0a, 0x05, 0x41, 0x70, 0x70, 0x49, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x41, 0x70, 0x70, 0x49, 0x64, 0x22, 0x22, 0x0a, 0x0a, 0x56,
	0x63, 0x54, 0x6f, 0x6b, 0x65, 0x6e, 0x52, 0x65, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x54, 0x6f, 0x6b,
	0x65, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x54, 0x6f, 0x6b, 0x65, 0x6e, 0x22,
	0xc1, 0x01, 0x0a, 0x13, 0x53, 0x61, 0x76, 0x65, 0x53, 0x69, 0x67, 0x6e, 0x31, 0x74, 0x6f, 0x31,
	0x41, 0x72, 0x67, 0x73, 0x52, 0x65, 0x71, 0x12, 0x14, 0x0a, 0x05, 0x54, 0x78, 0x6e, 0x49, 0x44,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x54, 0x78, 0x6e, 0x49, 0x44, 0x12, 0x12, 0x0a,
	0x04, 0x4f, 0x74, 0x63, 0x61, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x4f, 0x74, 0x63,
	0x61, 0x12, 0x1c, 0x0a, 0x09, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x4b, 0x65, 0x79, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x4b, 0x65, 0x79, 0x12,
	0x14, 0x0a, 0x05, 0x53, 0x6f, 0x74, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05,
	0x53, 0x6f, 0x74, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x54, 0x6f, 0x74, 0x69, 0x64, 0x18, 0x05,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x54, 0x6f, 0x74, 0x69, 0x64, 0x12, 0x10, 0x0a, 0x03, 0x53,
	0x61, 0x63, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x53, 0x61, 0x63, 0x12, 0x10, 0x0a,
	0x03, 0x54, 0x61, 0x63, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x54, 0x61, 0x63, 0x12,
	0x12, 0x0a, 0x04, 0x4f, 0x54, 0x49, 0x44, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x4f,
	0x54, 0x49, 0x44, 0x22, 0x2d, 0x0a, 0x13, 0x53, 0x61, 0x76, 0x65, 0x53, 0x69, 0x67, 0x6e, 0x31,
	0x74, 0x6f, 0x31, 0x41, 0x72, 0x67, 0x73, 0x52, 0x65, 0x73, 0x12, 0x16, 0x0a, 0x06, 0x52, 0x65,
	0x73, 0x75, 0x6c, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x52, 0x65, 0x73, 0x75,
	0x6c, 0x74, 0x22, 0xb3, 0x01, 0x0a, 0x0d, 0x56, 0x65, 0x72, 0x69, 0x66, 0x79, 0x31, 0x74, 0x6f,
	0x31, 0x52, 0x65, 0x71, 0x12, 0x14, 0x0a, 0x05, 0x54, 0x78, 0x6e, 0x49, 0x44, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x05, 0x54, 0x78, 0x6e, 0x49, 0x44, 0x12, 0x1a, 0x0a, 0x08, 0x48, 0x61,
	0x73, 0x68, 0x44, 0x61, 0x74, 0x61, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x48, 0x61,
	0x73, 0x68, 0x44, 0x61, 0x74, 0x61, 0x12, 0x18, 0x0a, 0x07, 0x4d, 0x61, 0x63, 0x53, 0x69, 0x67,
	0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x4d, 0x61, 0x63, 0x53, 0x69, 0x67, 0x6e,
	0x12, 0x18, 0x0a, 0x07, 0x48, 0x61, 0x73, 0x68, 0x53, 0x61, 0x62, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x07, 0x48, 0x61, 0x73, 0x68, 0x53, 0x61, 0x62, 0x12, 0x18, 0x0a, 0x07, 0x48, 0x61,
	0x73, 0x68, 0x54, 0x61, 0x62, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x48, 0x61, 0x73,
	0x68, 0x54, 0x61, 0x62, 0x12, 0x10, 0x0a, 0x03, 0x53, 0x61, 0x62, 0x18, 0x06, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x03, 0x53, 0x61, 0x62, 0x12, 0x10, 0x0a, 0x03, 0x54, 0x61, 0x62, 0x18, 0x07, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x03, 0x54, 0x61, 0x62, 0x22, 0x91, 0x01, 0x0a, 0x0d, 0x56, 0x65, 0x72,
	0x69, 0x66, 0x79, 0x31, 0x74, 0x6f, 0x31, 0x52, 0x65, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x54, 0x78,
	0x6e, 0x49, 0x44, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x54, 0x78, 0x6e, 0x49, 0x44,
	0x12, 0x18, 0x0a, 0x07, 0x48, 0x61, 0x73, 0x68, 0x53, 0x61, 0x63, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x07, 0x48, 0x61, 0x73, 0x68, 0x53, 0x61, 0x63, 0x12, 0x18, 0x0a, 0x07, 0x48, 0x61,
	0x73, 0x68, 0x54, 0x61, 0x63, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x48, 0x61, 0x73,
	0x68, 0x54, 0x61, 0x63, 0x12, 0x10, 0x0a, 0x03, 0x53, 0x61, 0x63, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x03, 0x53, 0x61, 0x63, 0x12, 0x10, 0x0a, 0x03, 0x54, 0x61, 0x63, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x03, 0x54, 0x61, 0x63, 0x12, 0x12, 0x0a, 0x04, 0x4f, 0x74, 0x69, 0x64,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x4f, 0x74, 0x69, 0x64, 0x32, 0x8e, 0x01, 0x0a,
	0x02, 0x56, 0x63, 0x12, 0x4c, 0x0a, 0x10, 0x53, 0x61, 0x76, 0x65, 0x53, 0x69, 0x67, 0x6e, 0x31,
	0x74, 0x6f, 0x31, 0x41, 0x72, 0x67, 0x73, 0x12, 0x1a, 0x2e, 0x76, 0x63, 0x2e, 0x76, 0x31, 0x2e,
	0x53, 0x61, 0x76, 0x65, 0x53, 0x69, 0x67, 0x6e, 0x31, 0x74, 0x6f, 0x31, 0x41, 0x72, 0x67, 0x73,
	0x52, 0x65, 0x71, 0x1a, 0x1a, 0x2e, 0x76, 0x63, 0x2e, 0x76, 0x31, 0x2e, 0x53, 0x61, 0x76, 0x65,
	0x53, 0x69, 0x67, 0x6e, 0x31, 0x74, 0x6f, 0x31, 0x41, 0x72, 0x67, 0x73, 0x52, 0x65, 0x73, 0x22,
	0x00, 0x12, 0x3a, 0x0a, 0x0a, 0x56, 0x65, 0x72, 0x69, 0x66, 0x79, 0x31, 0x74, 0x6f, 0x31, 0x12,
	0x14, 0x2e, 0x76, 0x63, 0x2e, 0x76, 0x31, 0x2e, 0x56, 0x65, 0x72, 0x69, 0x66, 0x79, 0x31, 0x74,
	0x6f, 0x31, 0x52, 0x65, 0x71, 0x1a, 0x14, 0x2e, 0x76, 0x63, 0x2e, 0x76, 0x31, 0x2e, 0x56, 0x65,
	0x72, 0x69, 0x66, 0x79, 0x31, 0x74, 0x6f, 0x31, 0x52, 0x65, 0x73, 0x22, 0x00, 0x42, 0x16, 0x5a,
	0x14, 0x61, 0x70, 0x69, 0x2f, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x77, 0x6f, 0x72, 0x6c, 0x64, 0x2f,
	0x76, 0x31, 0x3b, 0x76, 0x31, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_vc_v1_vc_proto_rawDescOnce sync.Once
	file_vc_v1_vc_proto_rawDescData = file_vc_v1_vc_proto_rawDesc
)

func file_vc_v1_vc_proto_rawDescGZIP() []byte {
	file_vc_v1_vc_proto_rawDescOnce.Do(func() {
		file_vc_v1_vc_proto_rawDescData = protoimpl.X.CompressGZIP(file_vc_v1_vc_proto_rawDescData)
	})
	return file_vc_v1_vc_proto_rawDescData
}

var file_vc_v1_vc_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_vc_v1_vc_proto_goTypes = []interface{}{
	(*VcTokenReq)(nil),          // 0: vc.v1.VcTokenReq
	(*VcTokenRes)(nil),          // 1: vc.v1.VcTokenRes
	(*SaveSign1To1ArgsReq)(nil), // 2: vc.v1.SaveSign1to1ArgsReq
	(*SaveSign1To1ArgsRes)(nil), // 3: vc.v1.SaveSign1to1ArgsRes
	(*Verify1To1Req)(nil),       // 4: vc.v1.Verify1to1Req
	(*Verify1To1Res)(nil),       // 5: vc.v1.Verify1to1Res
}
var file_vc_v1_vc_proto_depIdxs = []int32{
	2, // 0: vc.v1.Vc.SaveSign1to1Args:input_type -> vc.v1.SaveSign1to1ArgsReq
	4, // 1: vc.v1.Vc.Verify1to1:input_type -> vc.v1.Verify1to1Req
	3, // 2: vc.v1.Vc.SaveSign1to1Args:output_type -> vc.v1.SaveSign1to1ArgsRes
	5, // 3: vc.v1.Vc.Verify1to1:output_type -> vc.v1.Verify1to1Res
	2, // [2:4] is the sub-list for method output_type
	0, // [0:2] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_vc_v1_vc_proto_init() }
func file_vc_v1_vc_proto_init() {
	if File_vc_v1_vc_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_vc_v1_vc_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*VcTokenReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vc_v1_vc_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*VcTokenRes); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vc_v1_vc_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaveSign1To1ArgsReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vc_v1_vc_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaveSign1To1ArgsRes); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vc_v1_vc_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Verify1To1Req); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vc_v1_vc_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Verify1To1Res); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_vc_v1_vc_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_vc_v1_vc_proto_goTypes,
		DependencyIndexes: file_vc_v1_vc_proto_depIdxs,
		MessageInfos:      file_vc_v1_vc_proto_msgTypes,
	}.Build()
	File_vc_v1_vc_proto = out.File
	file_vc_v1_vc_proto_rawDesc = nil
	file_vc_v1_vc_proto_goTypes = nil
	file_vc_v1_vc_proto_depIdxs = nil
}
