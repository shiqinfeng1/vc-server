#!/bin/bash
SHDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
set -e
set -u

$SHDIR/stop-mysql.sh
$SHDIR/stop-redis.sh
