#!/bin/bash

SHDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $SHDIR/.env

if [ ! -d $REDIS_DATA_DIR ];then
    sudo mkdir -p $REDIS_DATA_DIR
    sudo chown $(whoami):$(whoami) -R $REDIS_DATA_DIR
    chmod 777 -R $REDIS_DATA_DIR
fi
if [ ! -d $REDIS_SLAVE_DATA_DIR ];then
    sudo mkdir -p $REDIS_SLAVE_DATA_DIR
    sudo chown $(whoami):$(whoami) -R $REDIS_SLAVE_DATA_DIR
    chmod 777 -R $REDIS_SLAVE_DATA_DIR
fi

echo "--------------------------------------"
echo "[redis] 运行镜像为 <$PRD_REDIS_IMAGE>. "
echo "[redis] 服务地址为 <0.0.0.0:$REDIS_SRV_PORT>. "
echo "[redis] 命令行连接示例: <redis-cli -h $HOST_IP -p $REDIS_SRV_PORT --pass $REDIS_PASSWOED>."
echo "[redis] 切换db: <命令行模式： select <db编号> >."
echo "[redis] 获取全部key: <命令行模式： get keys >."
echo "--------------------------------------"

# docker network create --subnet 100.0.0.0/24 --scope swarm mca-v2-subnet || true
cd $SHDIR
# echo -e "\n-----\nstop redis service ... "
# docker-compose --project-name infra3 -f docker-compose-redis.yml down 
# sleep 1
echo -e "\n-----\nstart redis service ... "
docker-compose --project-name chain-infra3 -f docker-compose-redis.yml up -d 
cd - > /dev/null 2>&1 &