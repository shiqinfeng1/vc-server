#ifndef _PFUNC_H
#define _PFUNC_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <libgen.h>

#define LOG_EN 0

#define LOG_WHITE 1	  //亮白色
#define LOG_GREEN 2	  //绿色
#define LOG_BLUE 3	  //蓝色
#define LOG_YELLOW 4  //黄色
#define LOG_CYAN 5	  //青色
#define LOG_PURPLE 6  //紫色
#define LOG_RED 7	  //红色
#define LOG_CYANRED 8 //红色(下划线)

#define LOG_WHITE_ACT "\033[0;1;37m"
#define LOG_GREEN_ACT "\033[0;1;32m"
#define LOG_BLUE_ACT "\033[0;1;34m"
#define LOG_YELLOW_ACT "\033[0;1;33m"
#define LOG_CYAN_ACT "\033[0;1;36m"
#define LOG_PURPLE_ACT "\033[0;1;35m"
#define LOG_RED_ACT "\033[0;1;31m"
#define LOG_CYANRED_ACT "\033[0;1;4;31m"

#define LOG_LEVEL_(x) LOG_LEVEL_##x
#define LOG_LEVEL_1 LOG_WHITE_ACT
#define LOG_LEVEL_2 LOG_GREEN_ACT
#define LOG_LEVEL_3 LOG_BLUE_ACT
#define LOG_LEVEL_4 LOG_YELLOW_ACT
#define LOG_LEVEL_5 LOG_CYAN_ACT
#define LOG_LEVEL_6 LOG_PURPLE_ACT
#define LOG_LEVEL_7 LOG_RED_ACT
#define LOG_LEVEL_8 LOG_CYANRED_ACT

#define LOG_LEVEL LOG_WHITE

#if (LOG_EN)
#define debug_log(level, format, ...)                                                                                                                                                        \
	do                                                                                                                                                                                       \
	{                                                                                                                                                                                        \
		if (level >= LOG_LEVEL && level <= LOG_CYANRED)                                                                                                                                      \
		{                                                                                                                                                                                    \
			char dateTime[64] = {0};                                                                                                                                                         \
			struct timeb stTimeb;                                                                                                                                                            \
			struct tm *ptm;                                                                                                                                                                  \
			ftime(&stTimeb);                                                                                                                                                                 \
			ptm = localtime(&stTimeb.time);                                                                                                                                                  \
			snprintf(dateTime, sizeof(dateTime), "%d-%d-%d %d:%d:%d.%03d", (ptm->tm_year + 1900), (ptm->tm_mon + 1), ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, stTimeb.millitm); \
			printf("%s<%s %s(%d) %s>: " format "\033[m", LOG_LEVEL_(level), dateTime, basename((char *)__FILE__), __LINE__, __func__, ##__VA_ARGS__);                                        \
		}                                                                                                                                                                                    \
	} while (0)

#define debug_data(level, addr, len)                                                                                                                                                         \
	do                                                                                                                                                                                       \
	{                                                                                                                                                                                        \
		if (level >= LOG_LEVEL && level <= LOG_CYANRED)                                                                                                                                      \
		{                                                                                                                                                                                    \
			char dateTime[64] = {0};                                                                                                                                                         \
			struct timeb stTimeb;                                                                                                                                                            \
			struct tm *ptm;                                                                                                                                                                  \
			ftime(&stTimeb);                                                                                                                                                                 \
			ptm = localtime(&stTimeb.time);                                                                                                                                                  \
			snprintf(dateTime, sizeof(dateTime), "%d-%d-%d %d:%d:%d.%03d", (ptm->tm_year + 1900), (ptm->tm_mon + 1), ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, stTimeb.millitm); \
			printf("%s<%s %s(%d) %s>: &%s %d bytes as:\n"                                                                                                                                    \
				   "\033[m",                                                                                                                                                                 \
				   LOG_LEVEL_(level), dateTime, basename((char *)__FILE__), __LINE__, __func__, #addr, len);                                                                                 \
			for (unsigned int d_len = 0; d_len < len; d_len++)                                                                                                                               \
			{                                                                                                                                                                                \
				printf("%s %02X"                                                                                                                                                             \
					   "\033[m",                                                                                                                                                             \
					   LOG_LEVEL_(level), addr[d_len]);                                                                                                                                      \
				if ((d_len + 1) % 16 == 0)                                                                                                                                                   \
				{                                                                                                                                                                            \
					printf("\n");                                                                                                                                                            \
				}                                                                                                                                                                            \
			}                                                                                                                                                                                \
			printf("\n");                                                                                                                                                                    \
		}                                                                                                                                                                                    \
	} while (0)

// inline void debug_dump(const char* file_path,const char* p_pcmBuf,int len)
#define debug_dump(level, file_path, p_pcmBuf, len)                                \
	do                                                                             \
	{                                                                              \
		if (level >= LOG_LEVEL && level <= LOG_CYANRED)                            \
		{                                                                          \
			FILE *fd = fopen(file_path, "a+");                                     \
			if (fd == NULL)                                                        \
			{                                                                      \
				debug_log(LOG_RED, "debug_dump file %s open error.\n", file_path); \
				return;                                                            \
			}                                                                      \
			fwrite(p_pcmBuf, 1, len, fd);                                          \
			fflush(fd);                                                            \
			fclose(fd);                                                            \
		}                                                                          \
	} while (0)
#else
#define debug_log(level, format, ...)
#define debug_data(level, addr, len)
#define debug_dump(level, file_path, p_pcmBuf, len)
#endif

#ifdef __cplusplus
}
#endif

#endif
