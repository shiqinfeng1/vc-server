/*
 Navicat Premium Data Transfer

 Source Server         : 72.84-2
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : 192.168.72.84:23306
 Source Schema         : chainvcdb

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 09/11/2023 08:28:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for vc_crt_storage
-- ----------------------------
DROP TABLE IF EXISTS `vc_crt_storage`;
CREATE TABLE `vc_crt_storage`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `key_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '证书id标识',
  `key_path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '私钥路径',
  `crt_path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '证书路径',
  `created_at` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `vc_crt_storage_key_id`(`key_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '证书存储表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of vc_crt_storage
-- ----------------------------
INSERT INTO `vc_crt_storage` VALUES (1, 'b8cf78ce2ea0bda33669fcd174fead5a915540a22294c1f651aa2a88c2dbaa59', '/home/user/workspace/chainmaker-jzsg/chainmaker-go/build/release/client/jz.key', '/home/user/workspace/chainmaker-jzsg/chainmaker-go/build/release/client/jz.crt', NULL, NULL);

-- ----------------------------
-- Table structure for vc_sign_args1to1
-- ----------------------------
DROP TABLE IF EXISTS `vc_sign_args1to1`;
CREATE TABLE `vc_sign_args1to1`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sotid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'otid的签名参数s',
  `totid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'otid的签名参数t',
  `sac` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'ac的签名参数s',
  `tac` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'ac的签名参数t',
  `otid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'otid值',
  `txn_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '交易id',
  `created_at` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `vc_sign_args1to1_txn_id`(`txn_id` ASC) USING BTREE,
  UNIQUE INDEX `vc_sign_args1to1_otid`(`otid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '签名参数1to1表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of vc_sign_args1to1
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
