#!/bin/bash

SHDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $SHDIR
echo -e "\n-----\nstop redis service ... "
docker-compose --project-name chain-infra3 -f docker-compose-redis.yml down 
cd - > /dev/null 2>&1 &
