#!/bin/bash

SHDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $SHDIR
echo -e "\n-----\nstop mysql service ... "
docker-compose --project-name chain-infra4 -f docker-compose-mysql.yml down 
cd - > /dev/null 2>&1 &
