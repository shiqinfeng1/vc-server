#!/bin/bash

SHDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $SHDIR/.env

if [ ! -d $MYSQL_DATA_DIR/data ];then
    sudo mkdir -p $MYSQL_DATA_DIR/data
    sudo chown $(whoami):$(whoami) -R $MYSQL_DATA_DIR
    chmod 777 -R $MYSQL_DATA_DIR
fi
if [ ! -d $MYSQL_BACKUP_DATA_DIR ];then
    sudo mkdir -p $MYSQL_BACKUP_DATA_DIR
    sudo chown $(whoami):$(whoami) -R $MYSQL_BACKUP_DATA_DIR
    chmod 777 -R $MYSQL_BACKUP_DATA_DIR
fi

echo "--------------------------------------"
echo "[mysql] 运行镜像为 <$PRD_MYSQL_IMAGE> . "
echo "[mysql] 服务地址为 <0.0.0.0:$MYSQL_PORT> . "
echo "[mysql] root用户/密码: $MYSQL_ROOT_USER/$MYSQL_ROOT_PASSWORD ."
echo "[mysql] 普通用户/密码: $MYSQL_USER/$MYSQL_PASSWORD ."
echo "[mysql] mysql数据存储路径: $MYSQL_DATA_DIR/data ."
echo "[mysql] mysql数据备份路径: $MYSQL_BACKUP_DATA_DIR ."
echo "--------------------------------------"

# docker network create --subnet 100.0.0.0/24 --scope swarm mca-v2-subnet || true
cd $SHDIR
# echo -e "\n-----\nstop mysql service ... "
# docker-compose --project-name infra4 -f docker-compose-mysql.yml down 
# sleep 1
echo -e "\n-----\nstart mysql service ... "
docker-compose --project-name chain-infra4 -f docker-compose-mysql.yml up -d 
cd - > /dev/null 2>&1 &