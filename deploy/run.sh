#!/bin/bash
set -e
set -u

SHDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

. $SHDIR/.env

port_is_used()
{
    local port=$1
    netstat -an |grep $port 
    if [ $? -eq  0 ]; then
        echo "【注意】端口号 $port 已使用，请更换为可用的端口号"
        exit 0
    fi
}
check_srv_port()
{
    echo 
    port_is_used $REDIS_SRV_PORT
    port_is_used $REDIS_SLAVE_SRV_PORT
    port_is_used $MYSQL_PORT
}

load_image()
{
    local image=$1
    local  old_image=$2
    echo -e "\n====>解压并加载镜像 $image ..."
    tar -zxvf ${image}.gz
    docker rmi $old_image
    docker load -i $image 
    rm -rf ${image}.gz
    rm -rf ${image}
    echo -e "====>完成!\n"
}

load_infra_image()
{
    cd $SHDIR
    if [[ -f "infra-images.tar.gz" ]]; then
        tar -zxvf infra-images.tar.gz 
        load_image ${TAR_REDIS_IMAGE} ${PRD_REDIS_IMAGE}
        load_image ${TAR_MYSQL_IMAGE} ${PRD_MYSQL_IMAGE}
    fi
    cd - > /dev/null 2>&1 &
}

if [ ! -e "/etc/localtime" ];then
    echo "/etc/localtime not exist"
    sudo ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
fi

load_infra_image
$SHDIR/start-redis.sh
$SHDIR/start-mysql.sh

echo "load hardware driver ..."
cat /proc/devices | grep xdma > /dev/null
if [ $? != 0 ]; then
    chmod +x $SHDIR/resource/load_driver.sh
    sudo $SHDIR/resource/load_driver.sh
    if [ ! $? == 0 ]; then
        echo "【告警】加载硬件板卡模块失败!!!"
        HASHCARD="NOTOK"
    else 
        HASHCARD="OK"
        echo "load hardware driver ok."
    fi
else 
    echo "hardware driver already installed."
fi

echo "start vc-server ..."

$SHDIR/yamlUpdate -yamlfile=$SHDIR/config/config.yaml -key=redis.default.address -value="${HOST_IP}:${REDIS_SRV_PORT}"
$SHDIR/yamlUpdate -yamlfile=$SHDIR/config/config.yaml -key=redis.default.pass -value="${REDIS_PASSWOED}"
$SHDIR/yamlUpdate -yamlfile=$SHDIR/config/config.yaml -key=database.default.link -value="mysql:${MYSQL_USER}:${MYSQL_PASSWORD}@tcp(${MYSQL_HOST}:${MYSQL_PORT})/chainvcdb?loc=Local&parseTime=true"
echo "update config ok."

LD_LIBRARY_PATH=$SHDIR/resource/lib $SHDIR/vc-server